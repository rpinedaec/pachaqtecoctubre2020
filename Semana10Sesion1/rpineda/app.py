from flask import Flask
from flask import render_template, request, redirect, url_for, abort
from forms import SignupForm, LoginForm, AddUsersForm
from flask_login import LoginManager, logout_user, current_user, login_user, login_required
#from models import users, User, get_user
from werkzeug.urls import url_parse
from Database import db
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from dotenv import load_dotenv
from pathlib import Path
import os

app = Flask(__name__)
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
app.config['SECRET_KEY'] = '7110c8ae51a4b5af97be6534caef90e4bb9bdcb3380af008f90b23a5d1616bf319bc298105da20fe'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:pachaqtec@localhost:3306/farmacia'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

login_manager = LoginManager(app)
login_manager.login_view = "login"

db.init_app(app)
migrate = Migrate(app, db)
from models.User import User
from models.Detalle import Detalle
from models.Fac import Fac
from models.Medicamento import Medicamento
from models.Receta import Receta
from models.TipoDocumento import TipoDocumento
from models.TipoUser import TipoUser

@app.route("/")
def index():
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    return render_template("index.html")


@app.route("/signup/", methods=["GET", "POST"])
def show_signup_form():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = SignupForm()
    error = None
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        tipouser = form.tipouser.data
        # Comprobamos que no hay ya un usuario con ese email
        user = User.get_by_email(email)
        if user is not None:
            error = f'El email {email} ya está siendo utilizado por otro usuario'
        else:
            # Creamos el usuario y lo guardamos
            user = User(name=name, email=email, tipouser_id=tipouser)
            user.set_password(password)
            user.save()
            # Dejamos al usuario logueado
            login_user(user, remember=True)
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('index')
            return redirect(next_page)
    return render_template("signup_form.html", form=form, error=error)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_by_email(form.email.data)
        if user is not None and user.check_password(form.password.data):
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('index')
            return redirect(next_page)
    return render_template('login_form.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/doctores', methods=["GET", "POST"])
def getDoctores():
    form = AddUsersForm()
    error = None
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        tipouser = 1
        # Comprobamos que no hay ya un usuario con ese email
        user = User.get_by_email(email)
        if user is not None:
            error = f'El email {email} ya está siendo utilizado por otro usuario'
        else:
            # Creamos el usuario y lo guardamos
            user = User(name=name, email=email, tipouser_id=tipouser)
            user.set_password(password)
            user.save()
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('getDoctores')
            return redirect(next_page)
    users = User.query.filter_by(tipouser_id=1)
    return render_template('doctores.html', users=users, form=form, error=error)


@app.route('/farmaceuticos', methods=["GET", "POST"])
def getFarmaceuticos():
    form = AddUsersForm()
    error = None
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        tipouser = 2
        # Comprobamos que no hay ya un usuario con ese email
        user = User.get_by_email(email)
        if user is not None:
            error = f'El email {email} ya está siendo utilizado por otro usuario'
        else:
            # Creamos el usuario y lo guardamos
            user = User(name=name, email=email, tipouser_id=tipouser)
            user.set_password(password)
            user.save()
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('getFarmaceuticos')
            return redirect(next_page)
    users = User.query.filter_by(tipouser_id=2)
    return render_template('farmaceuticos.html', users=users, form=form, error=error)


@app.route('/pacientes', methods=["GET", "POST"])
def getPacientes():
    form = AddUsersForm()
    error = None
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        tipouser = 3
        # Comprobamos que no hay ya un usuario con ese email
        user = User.get_by_email(email)
        if user is not None:
            user.name = name
            user.email = email
            user.set_password(password)
            user.update().values(name = name, email = email)
            user.save()
        else:
            # Creamos el usuario y lo guardamos
            user = User(name=name, email=email, tipouser_id=tipouser)
            user.set_password(password)
            user.save()
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('getPacientes')
            return redirect(next_page)
    users = User.query.filter_by(tipouser_id=3)
    return render_template('pacientes.html', users=users, form=form, error=error)


@login_manager.user_loader
def load_user(user_id):
    return User.get_by_id(int(user_id))

@app.route('/factura', methods=["GET", "POST"])
def getFactura():
    return render_template('factura.html')


if __name__ == "__main__":
    app.run(debug=True)