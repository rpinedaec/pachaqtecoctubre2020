from Database import db


class Detalle(db.Model):
    __tablename__ = 'farmacia_detalle'

    id = db.Column(db.Integer, primary_key=True)
    fac_id  = db.Column(db.Integer, db.ForeignKey('farmacia_fac.id', ondelete='CASCADE'), nullable=False)
    medicamento_id = db.Column(db.Integer, db.ForeignKey('farmacia_medicamento.id', ondelete='CASCADE'), nullable=False)
    cantidad = db.Column(db.Integer, nullable = False)
    igv = db.Column(db.Numeric(10,2), nullable = False)

    def __repr__(self):
        return f'<Detalle {self.id}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Detalle.query.get(id)

    @staticmethod
    def get_by_fac(id):
        fac = Fac.get_by_id(id)
        return Fac.query.filter_by(fac_id=fac.id).first()

