from Database import db



class Fac(db.Model):
    __tablename__ = 'farmacia_fac'

    id = db.Column(db.Integer, primary_key=True)
    cliente_id  = db.Column(db.Integer, db.ForeignKey('farmacia_user.id', ondelete='CASCADE'), nullable=False)
    tipodocumento_id = db.Column(db.Integer, db.ForeignKey('farmacia_tipodocumento.id', ondelete='CASCADE'), nullable=False)
    fecha = db.Column(db.DateTime(), nullable = False)
    subtotal = db.Column(db.Numeric(10,2), nullable = False)
    igv = db.Column(db.Numeric(10,2), nullable = False)
    total = db.Column(db.Numeric(10,2), nullable = False)

    def __repr__(self):
        return f'<Factura {self.id}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Fac.query.get(id)

    @staticmethod
    def get_by_cliente(email):
        user = User.get_by_email(email)
        return Fac.query.filter_by(cliente_id=user.id).first()

    @staticmethod
    def get_by_medico(email):
        user = User.get_by_email(email)
        return Fac.query.filter_by(cliente_id=user.id).first()
