from Database import db
from .User import User


class Receta(db.Model):
    __tablename__ = 'farmacia_receta'

    id = db.Column(db.Integer, primary_key=True)
    medico_id = db.Column(db.Integer, db.ForeignKey('farmacia_user.id', ondelete='CASCADE'), nullable=False)
    paciente_id  = db.Column(db.Integer, db.ForeignKey('farmacia_user.id', ondelete='CASCADE'), nullable=False)
    medicamento_id = db.Column(db.Integer, db.ForeignKey('farmacia_medicamento.id', ondelete='CASCADE'), nullable=False)
    dosis = db.Column(db.String(256), unique=True, nullable=False)
    frecuencia = db.Column(db.String(256), unique=True, nullable=False)
    is_entregada = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'<Receta {self.id}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Receta.query.get(id)

    @staticmethod
    def get_by_paciente(email):
        user = User.get_by_email(email)
        return Medicamento.query.filter_by(paciente_id=user.id).first()

    @staticmethod
    def get_by_medico(email):
        user = User.get_by_email(email)
        return Medicamento.query.filter_by(medico_id=user.id).first()
