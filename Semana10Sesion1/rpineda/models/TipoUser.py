from Database import db

class TipoUser(db.Model):
    __tablename__ = 'farmacia_tipouser'

    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return f'<TipoUSer {self.descripcion}>'

    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return TipoUser.query.get(id)

    @staticmethod
    def get_by_descripcion(descripcion):
        return TipoUser.query.filter_by(descripcion=descripcion).first()

