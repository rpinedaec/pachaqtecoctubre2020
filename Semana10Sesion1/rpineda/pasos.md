- python -m venv venv
- pip install -r requirements.txt 
- touch init.py
- touch models.py
- Programamos el modelo
- >>> from init import db
- >>> db.create_all()
- touch forms.py
- mkdir templates
- mkdir static
- touch .env
- export FLASK_APP="init.py"
- export FLASK_ENV="development"