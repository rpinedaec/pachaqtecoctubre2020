from django import forms
from .models import Posicion, Tipovehiculo

class PosicionForm(forms.ModelForm):

    class Meta:
        model = Posicion
        fields = ('vehiculo_id', 'estado_id','posicion', 'fecha')

class TipovehiculoForm(forms.ModelForm):

    class Meta:
        model = Tipovehiculo
        fields = ('descripcion',)