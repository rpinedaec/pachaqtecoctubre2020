from django.urls import path

from . import views
app_name = 'appcliente'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'), 
    path('add/', views.AddPosicion, name='add'),
    path('addTipoVehiculo/', views.AddTipoVehiculo, name='addTipoVehiculo'),
    
]