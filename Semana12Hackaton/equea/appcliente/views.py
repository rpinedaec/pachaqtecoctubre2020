from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.shortcuts import redirect
from .forms import PosicionForm, TipovehiculoForm

from .models import Posicion
# Create your views here.
class IndexView(generic.ListView):
    template_name = 'appcliente/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Posicion.objects.order_by('-fecha')[:50]

def AddPosicion(request):
    
    if request.method == "POST":
        form = PosicionForm(request.POST)
        if form.is_valid():
            posicion = form.save(commit=False)
            posicion.save()
            ##('account/profile/') 
            #return redirect('appcliente/success/')  # 4
            return render(request,'appcliente/posicion_detail.html')
    else:
        form = PosicionForm()
        return render(request, 'appcliente/add_posicion.html', {'form': form})

def posicion_detail(request, pk):
    form = PostForm(instance=posicion)
    return render(request, 'appcliente/post_edit.html', {'form': form})

def AddTipoVehiculo(request):
    
    if request.method == "POST":
        form = TipovehiculoForm(request.POST)
        if form.is_valid():
            posicion = form.save(commit=False)
            posicion.save()
            ##('account/profile/') 
            #return redirect('appcliente/success/')  # 4
            return render(request,'appcliente/posicion_detail.html')
    else:
        form = TipovehiculoForm()
        return render(request, 'appcliente/add_tipo_vehiculo.html', {'form': form})