from django.contrib import admin
from .models import Game, Clasificacion, ModoJuego, Developer, Control
# Register your models here.

admin.site.register(Game)
admin.site.register(Clasificacion)
admin.site.register(ModoJuego)
admin.site.register(Developer)
admin.site.register(Control)
