from django.db import models

# Create your models here.
class Game(models.Model):
    nombre=models.CharField(max_length=50) 
    descripcion = models.TextField(max_length=300)
    estreno = models.DateField() 
    
    def __str__(self):
        return self.nombre

class ModoJuego(models.Model):
    descripcion=models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

class Developer(models.Model):
    nombre=models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

class Clasificacion(models.Model):
    descripcion=models.CharField(max_length=50)
    def __str__(self):
        return self.descripcion

class Control(models.Model):
    descripcion=models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

class GameModoJuego(models.Model):
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    modojuego_id = models.ForeignKey(ModoJuego, on_delete=models.CASCADE)

class GameDeveloper(models.Model):
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    developer_id=models.ForeignKey(Developer, on_delete=models.CASCADE)

class GameClasificacion(models.Model):
    game_id= models.ForeignKey(Game, on_delete=models.CASCADE)
    clasificacion_id=models.ForeignKey(Clasificacion, on_delete=models.CASCADE)

class GameControl(models.Model):
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    control_id = models.ForeignKey(Control, on_delete=models.CASCADE)