from django.apps import AppConfig


class AppclientConfig(AppConfig):
    name = 'appclient'
