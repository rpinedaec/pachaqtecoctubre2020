from django import forms
from .models import Posicion, Tipovehiculo, Client

class PosicionForm(forms.ModelForm):

    class Meta:
        model = Posicion
        fields = ('vehiculo_id', 'estado_id','latitud', 'longitud', 'fecha')

class TipovehiculoForm(forms.ModelForm):

    class Meta:
        model = Tipovehiculo
        fields = ('descripcion',)

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('nombres','apellidos','edad', 'vehiculo_id',)