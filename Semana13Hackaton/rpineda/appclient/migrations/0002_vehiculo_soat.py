# Generated by Django 3.1.4 on 2021-01-02 14:02

import appclient.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appclient', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiculo',
            name='soat',
            field=models.FileField(default='', upload_to=appclient.models.content_file_name),
        ),
    ]
