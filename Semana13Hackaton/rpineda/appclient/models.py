from django.db import models
from django.core.files.storage import FileSystemStorage
from gdstorage.storage import GoogleDriveStorage

gd_storage = GoogleDriveStorage()

def content_file_name(instance, filename):
       return '/'.join(['content', filename])

class Tipovehiculo(models.Model):
    descripcion=models.CharField(max_length=45) 
   
    def __str__(self):
        return self.descripcion

class Estado(models.Model):
    descripcion=models.CharField(max_length=45) 
   
    def __str__(self):
        return self.descripcion 


class Vehiculo(models.Model):
    tipovehiculo_id = models.ForeignKey(Tipovehiculo, on_delete=models.CASCADE)
    placa=models.CharField(max_length=10)  
    descripcion = models.CharField(max_length=45) 
    foto = models.ImageField(upload_to='Vehiculo')
    soat = models.FileField(upload_to='docs', storage=gd_storage, default='')
    def __str__(self):
        return self.placa 

class Client(models.Model):
    nombres=models.CharField(max_length=45) 
    apellidos = models.CharField(max_length=45) 
    edad = models.IntegerField() 
    vehiculo_id = models.ManyToManyField(Vehiculo)
    
    def __str__(self):
        return self.nombres

class Posicion(models.Model):
    vehiculo_id = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)        
    estado_id = models.ForeignKey(Estado, on_delete=models.CASCADE)
    latitud = models.CharField(max_length=50,default='')
    longitud = models.CharField(max_length=50,default='')
    fecha=models.DateTimeField()

