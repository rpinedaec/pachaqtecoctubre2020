from django.urls import path

from . import views
app_name = 'appclient'
urlpatterns = [
    path('crear_cliente', views.crear_cliente.as_view(), name='crear_cliente'),
    path('actualizar_cliente/<int:pk>/', views.actualizar_cliente.as_view(), name='actualizar_cliente'),
    path('eliminar_cliente/<int:pk>/', views.eliminar_cliente.as_view(), name='eliminar_cliente'),
    path('listar_clientes/<int:pk>/', views.listar_clientes.as_view(), name='listar_clientes'),
]