from django.shortcuts import render
from django.views.generic import ListView, TemplateView, CreateView, UpdateView, DeleteView, DetailView
from django.urls import reverse_lazy
from .models import Client
from .forms import ClientForm
# Create your views here.

class crear_cliente(CreateView):
    model = Client
    form_class = ClientForm
    template_name = 'appclient/add_cliente.html'
    success_url = reverse_lazy('client:listar_clientes')

class actualizar_cliente(UpdateView):
    model = Client
    form_class = ClientForm
    template_name = 'appclient/add_cliente.html'
    success_url = reverse_lazy('appclient:listar_clientes')

class eliminar_cliente(DeleteView):
    model = Client
    form_class = ClientForm
    template_name = 'appclient/add_cliente.html'
    success_url = reverse_lazy('appclient:listar_clientes')

class listar_clientes(DetailView):
    model = Client

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['clients'] = Client.objects.all()
        return context