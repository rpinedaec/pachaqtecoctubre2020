from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.1', title='Instrumentos Musicales API',
    description='Instrumentos Musicales API',
)

ns = api.namespace('instrumentos', description='Instriments operations')

instrumento = api.model('Instrumentos', {
    'id': fields.Integer(readonly=True, description='The task unique identifier'),
    'tipo': fields.String(required=True, description='Tipo de Instrumento'),
    'cuerdas': fields.Integer(min=4,required=True, description='Cuerdas de Instrumento'),
    'modelo': fields.String(required=True, description='Modelo de Instrumento'),
    'nombre': fields.String(required=True, description='Nombre de Instrumento'),
    'marca': fields.String(required=True, description='Marca de Instrumento'),
})


class InstrumentoDAO(object):
    def __init__(self):
        self.counter = 0
        self.instrumentos = []

    def get(self, id):
        for instrumento in self.instrumentos:
            if instrumento['id'] == id:
                return instrumento
        api.abort(404, "instrumento {} doesn't exist".format(id))

    def create(self, data):
        instrumento = data
        instrumento['id'] = self.counter = self.counter + 1
        self.instrumentos.append(instrumento)
        return instrumento

    def update(self, id, data):
        instrumento = self.get(id)
        instrumento.update(data)
        return instrumento

    def delete(self, id):
        instrumento = self.get(id)
        self.instrumentos.remove(instrumento)


DAO = InstrumentoDAO()
DAO.create({'tipo': 'Bajo', 'cuerdas': 5, 'modelo': 'Thunderbird', 'nombre':'Thunderbird Goth', 'marca': 'Epiphone'})


@ns.route('/')
class InstrumentoList(Resource):
    '''Shows a list of all instrumentos, and lets you POST to add new tasks'''
    @ns.doc('list_instrumentos')
    @ns.marshal_list_with(instrumento)
    def get(self):
        '''List all tasks'''
        return DAO.instrumentos

    @ns.doc('create_instrumento')
    @ns.expect(instrumento)
    @ns.marshal_with(instrumento, code=201)
    def post(self):
        '''Crear nuevo instrumento'''
        return DAO.create(api.payload), 201


@ns.route('/<int:id>')
@ns.response(404, 'instrumento not found')
@ns.param('id', 'The task identifier')
class Instrumento(Resource):
    '''Show a single instrumento item and lets you delete them'''
    @ns.doc('get_instrumento')
    @ns.marshal_with(instrumento)
    def get(self, id):
        '''Fetch a given resource'''
        return DAO.get(id)

    @ns.doc('delete_instrumento')
    @ns.response(204, 'instrumento deleted')
    def delete(self, id):
        '''Delete a task given its identifier'''
        DAO.delete(id)
        return '', 204

    @ns.expect(instrumento)
    @ns.marshal_with(instrumento)
    def put(self, id):
        '''Update a task given its identifier'''
        return DAO.update(id, api.payload)


if __name__ == '__main__':
    app.run(debug=True)