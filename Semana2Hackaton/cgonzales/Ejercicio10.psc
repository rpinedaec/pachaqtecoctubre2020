Proceso FactorialDeN
	Definir N, factorial,aux Como Entero
	Escribir "Ingrese un numero, el cual desee calcular su factorial"
	Leer N
	factorial = 1
	si N = 0 Entonces
		Escribir "El factorial de 0 es 1"
	FinSi
	si N < 1 Entonces
		Escribir "No se permiten numeros negativos"
	SiNo
		para aux = 1 Hasta N
			factorial = factorial*aux
		FinPara
		Escribir "El factorial de ", N " es ",factorial
	FinSi
FinProceso
