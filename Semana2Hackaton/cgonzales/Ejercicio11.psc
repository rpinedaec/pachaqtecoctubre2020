Proceso TablasMultiplicar
	Definir tabla, auxUno, auxDos Como Entero
	Escribir "Diganos que tabla de multiplicar desea"
	Leer tabla
	Escribir "La tabla de multiplicar del ",tabla, ":"
	para auxUno = 1 hasta 10
		auxDos = auxUno * tabla
		Escribir auxUno, " X ", tabla, " = ", auxDos 
	FinPara
FinProceso
