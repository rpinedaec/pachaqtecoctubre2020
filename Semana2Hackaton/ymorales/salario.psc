Proceso obreros
	//- Realice un Pseudoc�digo que calcule la n�mina salarial neto, de unos obreros cuyo trabajo se paga en horas. El c�lculo se realiza de la siguiente forma:
	
	//> Condiciones
	
	//Las primeras 35 horas a una tarifa fija.
	//Las horas extras se pagan a 1.5 m�s de la tarifa fija.
	//Los impuestos a deducir de los trabajadores var�an, seg�n el sueldo mensual si el sueldo es menos a S./ 20,000.00 el sueldo es 
	//libre de impuesto y si es al contrario se cobrar� un 20% de impuesto.
	
	Definir ht Como Entero
	Definir ph Como Real
	Definir sb, he, imp, sueldo, sn Como Real
	
	Escribir "Ingrese las horas trabajadas"
	leer ht
	Escribir "Ingrese el precio por hora"
	Leer ph
	
	Si ht < 35 Entonces
		bonus = 0
		sueldo = ht * ph
		
	SiNo
		he = ht - 35
		bonus = ph*0.5*he
		sb = ht*ph
		sueldo = sb + bonus
	FinSi
	si sueldo < 20000 Entonces
		imp = 0
	SiNo
		imp = sueldo * 0.2
	FinSi
	sn = sueldo - imp
	
	Escribir "Sueldo Base: ", sb
	Escribir "Bonus Horas Extra: ",bonus
	Escribir "Impuestos: ",imp
	Escribir "Sueldo Neto: ",sn

FinProceso
