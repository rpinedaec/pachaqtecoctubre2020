'''
Escribir un método que lea 3 números los cuales significan una fecha (día, mes, año). Comprobar que sea válida la fecha, si no es valido que imprima un mensaje de error, y si es válida imprimir el mes con su nombre.
'''

def nombreMes(mes):
    switcher = {
        1: "Enero",
        2: "Febrero",
        3: "Marzo",
        4: "Abril",
        5: "Mayo",
        6: "Junio",
        7: "Julio",
        8: "Agosto",
        9: "Septiembre",
        10: "Octubre",
        11: "Noviembre",
        12: "Diciembre"
    }
    return switcher.get(mes, "Mes Invalido")

while True:
    try:
        anio = int(input("Digitar el año: "))
        mes = int(input("Digitar el mes: "))
        dia = int(input("Digitar el dia: "))
        
        if anio>=1900 and anio<=9999:
            if mes>=1 and mes<=12:
                if ((dia >= 1 and dia <= 31) and (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12)):
                    print(f"La fecha es valida: {dia}/{nombreMes(mes)}/{anio}")
                else:
                    if ((dia >= 1 and dia <= 30) and (mes == 4 or mes == 6 or mes == 9 or mes == 11)):
                        print(f"La fecha es valida: {dia}/{nombreMes(mes)}/{anio}")
                    else:
                        if ((dia>=1 and dia<=28) and (mes==2)):
                            print(f"La fecha es valida: {dia}/{nombreMes(mes)}/{anio}")
                        else:
                            if (dia == 29 and mes == 2 and (anio % 400 == 0  or (anio % 4 == 0 and anio % 100 != 0))):
                                print(f"La fecha es valida: {dia}/{nombreMes(mes)}/{anio}")
                            else:
                                print("El dia no es valido")
            else:
                print("El mes no es valido")
        else:
            print("El anio no es valido")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")


        