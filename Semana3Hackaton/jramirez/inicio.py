'''
Inicio del Programa
'''
import problemas

menu = True
8
while menu:
    try:
        prob = int(input("Escoja un problema (1 - 15) o digita 0 para salir: "))
        if(prob == 0):
            print("Saliendo del programa")
            menu = False
        if(prob == 1):
            peso = int(input("Digita tu peso en numeros "))
            edad = int(input("digita tu edad en numeros "))
            respuesta = problemas.problema1(edad, peso)
            print(f"la respuesta es {respuesta}")
        if(prob == 2):
            base = int(input("Ingresa la base del triangulo: "))
            altura = int(input("Ingresa la altura del triangulo: "))
            respuesta = problemas.problema2(base, altura)
            print(f"la respuesta del problema 2 es : {respuesta}")
        if(prob == 3):
            radio = int(input("Ingresa el radio del circulo : "))
            respuesta = problemas.problema3(radio)
            print(f"la respuesta del problema 3 es : {respuesta}")
        if(prob == 4):
            valor1 = int(input("Ingresa el primer valor  : "))
            valor2 = int(input("Ingresa el segundo valor  : "))
            respuesta = problemas.problema4(valor1, valor2)
            print(f"la respuesta del problema 4 es : {respuesta}")
        if(prob == 5):
            num1 = int(input("Ingresa el primer numero  : "))
            num2 = int(input("Ingresa el segundo numero  : "))
            num3 = int(input("Ingresa el tercer numero  : "))
            respuesta = problemas.problema5(num1, num2, num3)
            print(f"la respuesta del problema 5 es : {respuesta}")
        if(prob == 6):
            dia = int(input("Ingresa el dia  : "))
            mes = int(input("Ingresa el mes  : "))
            ano  = int(input("Ingresa el ano  : "))
            respuesta = problemas.problema6(dia, mes, ano)
            print(f"la respuesta del problema 6 es : {respuesta}")
        if(prob == 7):
            respuesta = problemas.problema7()
            print(f"la respuesta del problema 7 es : {respuesta}")
        if(prob == 8):
            respuesta = problemas.problema8()
            print(f"la respuesta del problema 8 es : {respuesta}")
        if(prob == 9):             
            print(f"la respuesta del problema 9 es : ")
            problemas.problema9()
        if(prob == 10):
            numero  = int(input("Ingresa un numero para calcular su factorial  : "))
            respuesta = problemas.problema10(numero)
            print(f"la respuesta del problema 10 es : Factorial de !{numero} es= {respuesta} \n")
        if(prob == 11):
            problemas.problema11()
        if(prob == 12):
            problemas.problema12()
        if(prob == 13):
            problemas.problema13()
        if(prob == 14):
            
            valorcompra  = int(input("Ingresa el monto de la compra  : "))
            respuesta = problemas.problema14(valorcompra)
            print(f"la respuesta del problema 14 es : {respuesta}")
        if(prob == 15):
            text = problemas.problema15()
            print(text)
    except Exception as error:
        print(f"Ha ocurrido un error: {error}")

        