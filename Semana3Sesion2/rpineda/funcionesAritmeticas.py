'''
Este modulo es para definir las operaciones aritmeticas
'''
def Suma(numero1, numero2):
    resultado = numero1 + numero2
    return resultado

def Resta(numero1, numero2):
    resultado = numero1 - numero2
    return resultado

def Multiplicacion(numero1, numero2):
    resultado = numero1 * numero2
    return resultado

def Division(numero1, numero2):
    resultado = numero1 / numero2
    return resultado

def DivisionEntera(numero1, numero2):
    resultado = numero1 // numero2
    return resultado

def Potenciacion(numero1, numero2):
    resultado = numero1 ** numero2
    return resultado

def Modulo(numero1, numero2):
    resultado = numero1 % numero2
    return resultado
