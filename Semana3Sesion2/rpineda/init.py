######## ESTRUCTURA DE DATOS #######
########### LISTAS ############
listaEnteros = [
    1, 2, 3,
    4, 5, 6,
    7, 8, 9 
    ]
print(listaEnteros)
listaStrings = ["Roberto", "David", "Pineda", "Lopez"]
print(listaStrings)
listaCombinada = ["Karen", 24, True, 1.70]
print(listaCombinada)
print(listaCombinada[3])
#Agregar un elemento a la lista
listaCombinada.append("Chiclayo")
print(listaCombinada)
nuevaLista = ["Roberto", "Pineda"]
listaCombinada.extend(nuevaLista)
print(listaCombinada)
listaCombinada.remove(True)
print(listaCombinada)
print(listaCombinada.index("Chiclayo"))
listaCombinada.pop(2)
print(listaCombinada)
print(len(listaCombinada))
listaCombinada.extend(["David","David","David"])
print(listaCombinada)
listaCombinada.remove("David")
print(listaCombinada)
########## TUPLAS ############
tuplaCombinada = (1, "Karen", "Chiclayo", "Karen")
print(tuplaCombinada)
print(len(tuplaCombinada)) #.count(True))
print(tuplaCombinada.count("Karen"))
print(tuplaCombinada.index("Chiclayo"))
nuevaTupla = tuple(range(5,15))
print(nuevaTupla)
terceraTupla = (1, 2, nuevaTupla)
print(terceraTupla)
######### DICCIONARIOS #########33
midiccionario = {
    "Do": 32.7,
    "Re": 36.71,
    "Mi": 42.2,
    "Fa": 43.65,
    "Sol": 49,
    "La": 110,
    "Si": 61.44
    }
print(midiccionario)
print(midiccionario["Fa"])
print(type(midiccionario["Fa"]))
print(type(midiccionario["Sol"]))
print(type(midiccionario["La"]))
dicNotasMusicales = {
    "Octava1":{
        "Do": 32.7,
        "Re": 36.71,
        "Mi": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 110,
        "Si": 61.44
    },
    "Octava2":{
        "Do": 32.7,
        "Re": 36.71,
        "Mi": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 220,
        "Si": 61.44
    },
    "Octava3":{
        "Do": 32.7,
        "Re": 36.71,
        "Mi": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 330,
        "Si": 61.44
    },
    "Octava4":{
        "Do": 32.7,
        "Re": 36.71,
        "Mi": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 440,
        "Si": 61.44
    },
}
print(dicNotasMusicales["Octava3"])
print(dicNotasMusicales["Octava1"])
print(dicNotasMusicales["Octava3"]["Do"])
dicOctava5 = { "Octava5":
                {  
                    "Do": 32.7,
                    "Re": 36.71,
                    "Mi": 42.2,
                    "Fa": 43.65,
                    "Sol": 49,
                    "La": 550,
                    "Si": 61.44
                }
            }
dicNotasMusicales.update(dicOctava5)
print(dicNotasMusicales)