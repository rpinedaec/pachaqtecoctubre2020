# ➔ Crea una lista con los siguientes valores 1,3,20,10,50,100,31,1000
listNum = [1,3,20,10,50,100,31,1000]
# ➔ Itera los valores e imprime en consola el valor si es igual a 3
for i in listNum:
    if (i == 3):
        print(3)
        break
#
# ➔ Itera los valores e imprime un mensaje es par si el número es un número par:
for i in listNum:
    if ((i % 2) == 0):
        print(f"El numero {i} es par")
#
# ➔ Itera los valores y haz un break si el número es igual a 50
for i in listNum:
    print(i, end=" , ")
    if (i == 50):
        break
##
from os import system
system("clear")
system("clear")
