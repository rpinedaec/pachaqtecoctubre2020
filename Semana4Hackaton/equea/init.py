# -*- encoding: utf-8 -*- #
from modelos import Libro, Recibir, Devolvio, Pidio, Presto, Menu, FileManager
import os
import shutil
import json
from time import sleep

#####################################################################################

filePresto = FileManager("PrestoCliente.txt")
fileRecibir = FileManager("RecibirCliente.txt")
filePidio = FileManager("PidioAlumno.txt")
fileDevolvio = FileManager("DevolvioAlumno.txt")
lstRecibir = []
lstRecibirDic = []
lstPidio = []
lstPidioDic = []
lstPresto = []
lstPrestoDic = []
lstDevolvio = []
lstDevolvioDic = []


def cargaInicial():
    try:
        try:
            res = filePresto.leerArchivo()
            lstPresto = json.loads(res)
            for dicPresto in lstPresto:
                # codLibro, tituloLibro, autorLibro, IBNLibro, cantidadLibro
                objPresto = Presto(dicPresto["codLibro"], dicPresto["titulo"],
                                   dicPresto["autor"],
                                   dicPresto["IBN"], dicPresto["cantidad"])
                lstPresto.append(objPresto)
                lstPrestoDic.append(dicPresto)

        except FileNotFoundError as error:
            filePresto.escribirArchivo("")

        except Exception as error:
            print(error)

        try:

            res = ""
            res = fileRecibir.leerArchivo()

            lstRecibirtmp = json.loads(res)
            for dicRecibir in lstRecibirtmp:
                # codLibro, tituloLibro, autorLibro, IBNLibro, cantidadLibro
                objRecibir = Recibir(dicRecibir["codLibro"], dicRecibir["titulo"],
                                     dicRecibir["autor"],
                                     dicRecibir["IBN"], dicRecibir["cantidad"])
                lstRecibir.append(objRecibir)
                lstRecibirDic.append(dicRecibir)
        except FileNotFoundError as error:
            fileRecibir.escribirArchivo("")
        except Exception as error:
            print(error)

        try:

            res = ""
            res = filePidio.leerArchivo()
            lstPidio = json.loads(res)
            for dicPidio in lstPidio:
                # codLibro, tituloLibro, autorLibro, IBNLibro, cantidadLibro
                objPidio = Pidio(dicPidio["codLibro"], dicPidio["titulo"],
                                 dicPidio["autor"],
                                 dicPidio["IBN"], dicPidio["cantidad"])
                lstPidio.append(objPidio)
                lstPidioDic.append(dicPidio)
        except FileNotFoundError as error:
            filePidio.escribirArchivo("")
        except Exception as error:
            print(error)

        res = ""
        res = fileDevolvio.leerArchivo()
        lstDevolvio = json.loads(res)
        for dicDevolvio in lstPidio:
            # codLibro, tituloLibro, autorLibro, IBNLibro, cantidadLibro
            objDevolvio = Devolvio(dicDevolvio["codLibro"], dicDevolvio["titulo"],
                                   dicDevolvio["autor"],
                                   dicDevolvio["IBN"], dicDevolvio["cantidad"])
            lstDevolvio.append(objDevolvio)
            lstDevolvioDic.append(dicDevolvio)
    except FileNotFoundError as error:
        fileDevolvio.escribirArchivo("")
    except Exception as error:
        print(error)


# def buscarobjeto(nombreobjeto, lstbuscar, strnombrebuscar):
#     for objbuscar in lstbuscar:
#         if objbuscar[nombreobjeto] == strnombrebuscar:
#             return objbuscar
#         else:
#             return False


# def validarobjeto(nombreobjeto, lstvalidar, strnombrevalidar):
#     for objvalidar in lstvalidar:
#         if objvalidar[nombreobjeto] == strnombrevalidar:
#             return True
#         else:
#             return False


#############################


cargaInicial()

dicOpcionesMenuPrincipal = {"Bibliotecario": 1, "Alumno": 2}
menuPrincipal = Menu("Menu de Inicio", dicOpcionesMenuPrincipal)
opcionMenuPrincipal = menuPrincipal.MostrarMenu()


dicOpcionesPedirDevolverLibro = {"Pedir Libro": 1, "Devolver Libro": 2}
menuAlumnosub = Menu("Menu Alumno", dicOpcionesPedirDevolverLibro)

dicOpcionesPrestarRecibirLibro = {"Prestar Libro": 1, "Recibir Libro": 2}
menuBibliotecariosub = Menu("Menu Bibliotecario", dicOpcionesPrestarRecibirLibro)

# opcionMenuPrincipal = menuPrincipal.MostrarMenu()

if(opcionMenuPrincipal == 9):
    opsalir = False

elif(opcionMenuPrincipal == 1):
    dicOpcionesPrestarRecibirLibro = {"Prestar Libro": 1, "Recibir Libro": 2}
    menuBibliotecario = Menu("Menu Bibliotecario",
                             dicOpcionesPrestarRecibirLibro)
    resmenuBibliotecario = menuBibliotecario.MostrarMenu()
    salirCreacionBibliotecario = True
    try:
        while salirCreacionBibliotecario:
            if(resmenuBibliotecario == 1):

                print("Digita el Codigo del Libro")
                codLibro = input()
                print("Digita el Titulo del Libro")
                titulo = input()
                print("Digita el Autor del Libro")
                autor = input()
                print("Digita el IBN del Libro")
                IBN = input()
                print("Digita la Cantidad de Libros")
                cantidad = input()

                Presto = Presto(codLibro, titulo,
                                autor, IBN, cantidad)
                print("haz prestado el libro: ", Presto)
                filePresto.borrarArchivo()
                lstPrestoDic.append(Presto.dictPresto())
                lstPresto.append(Presto)
                jsonStr = json.dumps(lstPrestoDic)
                filePresto.escribirArchivo(jsonStr)
                resmenuBibliotecariosub = menuBibliotecariosub.MostrarMenu()
                if(resmenuBibliotecariosub == 1):
                    print("ingreso a la opcion 1 del menu Bibliotecario")
                elif(resmenuBibliotecariosub == 2):
                    print("ingreso a la opcion 2 del menu Bibliotecario")
                    for objPresto in lstPresto:
                        print(
                            f"|{objPresto.codLibro} | {objPresto.titulo} | {objPresto.autor} | {objPresto.IBN} | {objPresto.cantidad} |")
                    sleep(10)
                    resretornarmenuBibliotecario = menuBibliotecario.MostrarMenu()
                    if(resretornarmenuBibliotecario == 1):
                        print(
                            f"ingreso a la opcion {resretornarmenuBibliotecario}")
                else:
                    print(
                        f"ingreso a la opcion {resmenuBibliotecariosub} de menu Bibliotecario")
                    salirCreacionBibliotecario = False
                    break
            elif(resmenuBibliotecario == 2):

                print("Digita el Codigo del Libro")
                codLibro = input()
                print("Digita el Titulo del Libro")
                titulo = input()
                print("Digita el Autor del Libro")
                autor = input()
                print("Digita el IBN del Libro")
                IBN = input()
                print("Digita la Cantidad de Libros")
                cantidad = input()

                Recibir = Recibir(codLibro, titulo,
                                  autor, IBN, cantidad)
                print("haz recibido el libro: ", Recibir)
                fileRecibir.borrarArchivo()
                lstRecibirDic.append(Recibir.dictRecibir())
                lstRecibir.append(Recibir)
                jsonStr = json.dumps(lstRecibirDic)
                fileRecibir.escribirArchivo(jsonStr)
                resmenuBibliotecariosub = menuBibliotecariosub.MostrarMenu()
                if(resmenuBibliotecariosub == 1):
                    print("ingreso a la opcion 1 del menu Bibliotecario")
                elif(resmenuBibliotecariosub == 2):
                    print("ingreso a la opcion 2 del menu Bibliotecario")
                    for objRecibir in lstRecibir:
                        print(
                            f"|{objRecibir.codLibro} | {objRecibir.titulo} | {objRecibir.autor} | {objRecibir.IBN} | {objRecibir.cantidad} |")
                    sleep(10)
                    res = menuBibliotecario.MostrarMenu()
                else:
                    print("No se encuentra el libro a buscar")
                # break
            else:
                pass
    except Exception as error:
        print(error)

elif(opcionMenuPrincipal == 2):
    dicOpcionesPedirDevolverLibro = {"Pedir Libro": 1, "Devolver Libro": 2}
    menuAlumno = Menu("Menu Alumno", dicOpcionesPedirDevolverLibro)
    resmenuAlumno = menuAlumno.MostrarMenu()
    salirCreacionAlumno = True
    try:
        while salirCreacionAlumno:
            if(resmenuAlumno == 1):

                print("Digita el Codigo del Libro")
                codLibro = input()
                print("Digita el Titulo del Libro")
                titulo = input()
                print("Digita el Autor del Libro")
                autor = input()
                print("Digita el IBN del Libro")
                IBN = input()
                print("Digita la Cantidad de Libros")
                cantidad = input()

                Pidio = Pidio(codLibro, titulo,
                              autor, IBN, cantidad)
                print("haz pedido el libro: ", Pidio)
                filePidio.borrarArchivo()
                lstPidioDic.append(Pidio.dictPidio())
                lstPidio.append(Pidio)
                jsonStr = json.dumps(lstPidioDic)
                filePidio.escribirArchivo(jsonStr)
                resmenuAlumnosub = menuAlumnosub.MostrarMenu()
                if(resmenuAlumnosub == 1):
                    print("ingreso a la opcion 1 del menu Alumno")
                elif(resmenuAlumnosub == 2):
                    print("ingreso a la opcion 2 del menu Alumno")
                    for objPidio in lstPidio:
                        print(
                            f"|{objPidio.codLibro} | {objPidio.titulo} | {objPidio.autor} | {objPidio.IBN} | {objPidio.cantidad} |")
                    sleep(10)
                    resretornarmenuAlumno = menuAlumno.MostrarMenu()
                    if(resretornarmenuAlumno == 1):
                        print(f"ingreso a la opcion {resretornarmenuAlumno}")
                else:
                    print(
                        f"ingreso a la opcion {resmenuAlumnosub} de menu Alumno")
                    salirCreacionAlumno = False
                    break
            elif(resmenuAlumno == 2):

                print("Digita el Codigo del Libro")
                codLibro = input()
                print("Digita el Titulo del Libro")
                titulo = input()
                print("Digita el Autor del Libro")
                autor = input()
                print("Digita el IBN del Libro")
                IBN = input()
                print("Digita la Cantidad de Libros")
                cantidad = input()

                Devolvio = Devolvio(codLibro, titulo,
                                    autor, IBN, cantidad)
                print("haz devuelto el libro: ", Devolvio)
                fileDevolvio.borrarArchivo()
                lstDevolvioDic.append(Devolvio.dictDevolvio())
                lstDevolvio.append(Devolvio)
                jsonStr = json.dumps(lstDevolvioDic)
                fileDevolvio.escribirArchivo(jsonStr)
                resmenuAlumnosub = menuAlumnosub.MostrarMenu()
                if(resmenuAlumnosub == 1):
                    print("ingreso a la opcion 1 del menu Alumno")
                elif(resmenuAlumnosub == 2):
                    print("ingreso a la opcion 2 del menu Alumno")
                    for objDevolvio in lstDevolvio:
                        print(
                            f"|{objDevolvio.codLibro} | {objDevolvio.titulo} | {objDevolvio.autor} | {objDevolvio.IBN} | {objDevolvio.cantidad} |")
                    sleep(10)
                    res = menuAlumno.MostrarMenu()
                else:
                    print("No se encuentra el libro a buscar")
                # break
            else:
                pass
    except Exception as error:
        print(error)


# print("Bienvenido a la Biblioteca de Pachaqtec")


# opciones = {"Alumno": 1, "Bibliotecario": 2,}
# menu = Menu("Menu Principal", opciones)

# opcion = menu.MostrarMenu()
# print(opcion)
# if(opcion == 1):
#     alumnoOpciones = {"Deseas un libro":1, "Devuelves un libro":2}
#     nuevoMenu = Menu("Alumno", alumnoOpciones)
#     respuesta = nuevoMenu.MostrarMenu()
#     if(respuesta == 1):
#         print("Eres del Alianza")
#     elif(respuesta == 2):
#         print("Eres de Univesitario")
#     else:
#         print("No hat opcion")

# if(opcion == 2):
#     bibliotecarioOpciones = {"Prestar un libro":1, "Recibir un libro":2, "Buscar un libro":3}
#     nuevoMenu = Menu("Bibliotecario", bibliotecarioOpciones)
#     respuesta = nuevoMenu.MostrarMenu()
#     if(respuesta == 1):
#         print("Eres del Alianza")
#     elif(respuesta == 2):
#         print("Eres de Univesitario")
#     elif(respuesta == 3):
#         print("Eres de Univesitario")
#     else:
#         print("No hat opcion")
