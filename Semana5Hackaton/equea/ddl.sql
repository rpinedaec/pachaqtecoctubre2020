create schema equea;

create table alumnos(
    idalumnos serial,
    nombre varchar(100) not null,
    apellidopaterno varchar(100) not null,
    apellidomaterno varchar(100) not null,
    dni int,
    edad int,
    constraint pk_alumnos primary key (idalumnos)
);

create table profesores(
    idprofesores serial,
    nombre varchar(100) not null,
    apellidopaterno varchar(100) not null,
    apellidomaterno varchar(100) not null,
    dni int,
    edad int,
    constraint pk_profesores primary key (idprofesores)
);

create table salon(
    idsalon serial,
	idprofesores serial,
    nombre varchar(100) not null,
    añoescolar int,
    constraint pk_salon primary key (idsalon),
    constraint fk_salon_profesores
    foreign key (idprofesores)
    references profesores (idprofesores)
    ON DELETE cascade
    ON UPDATE cascade
);

create table cursos(
    idcursos serial,
	idsalon serial,
    nombre varchar(100) not null,
    profesor varchar(100) not null,
    constraint pk_cursos primary key (idcursos),
    constraint fk_cursos_salon
    foreign key (idsalon)
    references salon (idsalon)
    ON DELETE cascade
    ON UPDATE cascade 
);

create table notasbimestrales(
    idalumnos serial,
    idcursos serial,
    notasbimestrales int,
    constraint pk_notasbimestrales primary key (idalumnos, idcursos),
    constraint fk_notasbimestrales_alumnos
    foreign key (idalumnos)
    references alumnos (idalumnos)
    ON DELETE cascade
    ON UPDATE cascade,
    constraint fk_notasbimestrales_cursos
    foreign key (idcursos)
    references cursos (idcursos)
    ON DELETE cascade
    ON UPDATE cascade
);