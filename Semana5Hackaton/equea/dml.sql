insert into alumnos(nombre, apellidopaterno, apellidomaterno, dni, edad)
values  ('Roberto','Pineda','Alcides','001575291',37),
        ('David','Lopez','Gomes','1716861990',27),
		('Edwar','Quea','Huanqui','45673921',38);
		
update alumnos
set apellidomaterno = 'Lopez' where idalumnos = 1;

select * from alumnos ;

delete from alumnos where idalumnos = 2;

insert into profesores(nombre, apellidopaterno, apellidomaterno, dni, edad)
values  ('Andres','Ramirez','Alcides','0298390',26),
        ('Victor','Lopez','Gomes','89282930',20),
		('Francisco','Quea','Huanqui','1782930',30);

update profesores
set apellidomaterno = 'Alcides' where idprofesores = 1;

select * from profesores ;

delete from profesores where idprofesores = 2;

insert into salon (idprofesores, nombre, añoescolar)
values  (1,'01',2),
        (2,'02',3),
		(3,'03',1);
		
update salon
set nombre = '01' where idsalon = 1;

select * from salon ;

delete from salon where idsalon = 2;
		
insert into cursos (idsalon, nombre, profesor)
values  (1,'Matematicas_1','Roberto'),
        (2,'Fisica_2','David'),
		(3,'Programcion_3','Edwar');
		
update cursos
set nombre = '01' where idcursos = 1;

select * from cursos ;

delete from cursos where idcursos = 2;	

insert into notasbimestrales (idalumnos, idcursos, notasbimestrales)
values  (1,2,15),
        (2,1,20),
		(3,2,14);
		
update notasbimestrales
set notasbimestrales = '01' where idalumnos = 1;

select * from notasbimestrales ;

delete from notasbimestrales where idcursos = 2;	