
  

# Hackatón Semana 5
## **LOGRO**: SQL y Modelos Entidad-Relación
**“Empecemos a programar!!!”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto:
![](bdd.jpg)
**Resuelve el problema en SQL y súbelo a Gitlab**
### Insumos para resolver el Reto
 - Tener instalado Visual Studio Code 
 - Tener conocimientos básicos de SQL 
 - Tener conocimientos sobre Base de Datos
 
### Pasos a seguir para resolver el Reto
 - Descargar la ultima versión del proyecto
 - Crear la carpeta personal en la carpeta Semana5Hackaton
 - Resolver los problemas propuestos y guardarlos en varios archivos diferentes en la carpeta personal
 - Debe estar optimizado para **PostgreSQL**
 - Sube tus cambios a tu repositorio git.
 - Envía el **merge request** para integrar los cambios
### Solución del Reto
El proyecto debe tener una carpeta personal donde vamos a colocar programación requerida.
El problema a resolver es el siguiente:
- Crearemos un programa donde tengamos en cuenta lo siguiente: 
	 - Alumnos: nombre, identificador, edad, correo 
	 - Salón: nombre, año escolar 
	 - Cursos: nombre, profesor 
	 - Profesores: nombre, identificador, edad, correo 
 - Se deben tener en cuenta las siguiente pautas: 
	 - Se tiene que cada año escolar puede tener bimestre, de las cuales por salón se tienen a profesores asignados a cursos específicos 
	 - Alumno y profesor están asociados a un salón 
	 - El alumno tiene un registro de notas bimestrales (no de exámenes o pruebas) 
	 - El profesor puede cambiarse de curso en un salón específico

 - El repositorio deberá contar con commit's y su push en la rama principal (master)
### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos:
 - Que se tenga en el gitlab 1 carpeta con el nombre (inicial del nombre y el apellido del estudiante) Dentro de ella, se agregarán los archivos de SQL y el modelo los datos de las entidades
 - Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto.
 - Revisar que contengan commits y el push en la rama principal.


https://gitlab.com/rpinedaec/pachaqtecoctubre2020.git
