insert into alumno(nombre, apellido_paterno, apellido_materno, dni, edad, correo)
values  ('Roberto','Pineda',null,'001575291',37,'rpineda@x-codec.net'),
        ('David','Lopez',null,'1716861990',27,'dlopez@x-codec.net');

update alumno
set apellido_materno = 'Lopez' where idalumno = 1;

select * from alumno ;

delete from alumno where idalumno = 2;