from utils import  Conexion
import utils
from modelos import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle

lstCliente = lstMesero = lstMenu = lstMesa = lstOrden = lstOrdenDetalle = []
query = ''

def CargaInicial():
    conn = Conexion()
    query = 'Select * from cliente'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Cliente(objResult[0],objResult[1],objResult[2] )
        lstCliente.append(objClass)
    
    query = 'Select * from mesero'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Mesero(objResult[0],objResult[1],objResult[2])
        lstMesero.append(objClass)
    
    query = 'Select * from menu'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Menu(objResult[0],objResult[1],objResult[2] )
        lstMenu.append(objClass)

    query = 'Select * from mesa'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Mesa(objResult[0],objResult[1] )
        lstMesa.append(objClass)

    query = 'Select * from ordencabecera'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Orden(objResult[0],objResult[1],objResult[2],objResult[3],objResult[4] )
        lstOrden.append(objClass)
    
    query = 'Select * from ordendetalle'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = OrdenDetalle(objResult[0],objResult[1],objResult[2],objResult[2])
        lstOrdenDetalle.append(objClass)

CargaInicial()

opciones = {"Realizar Pedido" : 1, "Mantenedor": 2}
menuPrincipal = utils.Menu("Menu Principal",opciones)
result = menuPrincipal.MostrarMenu()
if result == 1: ######## Opcion Realizar Pedido #############
    print("estoy en Realizar pedido")
    
if result == 2: ######## Opcion Mantenedor #############
    opciones = {"Clientes":1, "Mesero": 2, "Menu": 3, "Mesas": 4 }
    menuMantenedor = utils.Menu("Menu de Mantenedores", opciones)
    result = menuMantenedor.MostrarMenu()
    if result == 1: ####### menu clientes ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuCliente = utils.Menu("Menu de Clientes", opciones)
        result = menuCliente.MostrarMenu()
        if result == 1: ####### Cliente - Crear #####
            nombre = input("Escribe el Nombre del Cliente: ")
            apellido = input("Escribe el Apellido del Cliente: ")
            query = f"insert into cliente (nombre, apellido ) values('{nombre}','{apellido}')"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Cliente Creado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==2:  ####### Cliente - Buscar #####
            nombre = input("Escribe el Nombre del Cliente a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from cliente where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        if result == 3: ####### Cliente - Actualizar  #####
            nombre = input("Escribe el Nombre del Cliente a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from cliente where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idCliente = int(input("escoje el id del cliente que deseas Actualizar"))
            nombre = input("Escribe del nuevo Nombre del Cliente a actualizar: ")
            apellido = input("Escribe del nuevo  Apellido del Cliente a actualizar : ")
            query = f"update cliente set nombre = '{nombre}', apellido = '{apellido}' where idcliente = {idCliente}"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Cliente Actualizado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==4: ####### Cliente - Borrar  #####
            nombre = input("Escribe el nombre del Cliente a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from cliente where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idCliente = int(input("escoje el id del cliente que deseas Borrar : "))
            query = f"delete from cliente where idcliente = {idCliente};"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Borrado Correctamente")
                lstCliente = []
                CargaInicial()


    if result == 2: ####### menu mesero ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMesero = utils.Menu("Menu Mesero", opciones)
        result = menuMesero.MostrarMenu()
        
        if result == 1: ####### Mesero - Crear #####
            nombre = input("Escribe el Nombre del Mesero: ")
            apellido = input("Escribe el Apellido del Mesero: ")
            query = f"insert into mesero (nombre, apellido ) values('{nombre}','{apellido}')"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Creado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==2:  ####### Mesero - Buscar #####
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from mesero where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
         
        if result == 3: ####### Mesero - Actualizar  #####
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from mesero where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idmesero = int(input("escoje el id del mesero que deseas Actualizar"))
            nombre = input("Escribe del nuevo Nombre del Mesero a actualizar: ")
            apellido = input("Escribe del nuevo  Apellido del Mesero a actualizar : ")
            query = f"update mesero set nombre = '{nombre}', apellido = '{apellido}' where idmesero = {idmesero}"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Actualizado Correctamente")
                lstCliente = []
                CargaInicial()
        
        if result ==4: ####### Mesero - Borrar  #####
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from mesero where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idmesero = int(input("escoje el id del mesero que deseas Borrar "))
            query = f"delete from mesero where idmesero = {idmesero};"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Borrado Correctamente")
                lstCliente = []
                CargaInicial()


    if result == 3: ####### Menu de Restaurant ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMenu = utils.Menu("Menu ", opciones)
        result = menuMenu.MostrarMenu()
        
        if result == 1: ####### Menu #####
            nombre = input("Escribe el Nombre del plato a solicitar: ")
            valor = int(input("Escribe el precio del plato: "))
            query = f"insert into menu (nombre, valor) values('{nombre}','{valor}');"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Creado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==2:  ####### Menu - Buscar #####
            nombre = input("Escribe el Nombre del Plato a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from menu where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
        if result == 3: ####### Menu- Actualizar  #####
            nombre = input("Escribe el Nombre del plato a actualizar: ")
            nombre = nombre.capitalize()
            query = f"select * from menu where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idmenu = int(input("escoje el id del plato que deseas Actualizar"))
            nombre = input("Escribe del nuevo Nombre del Plato que deseas actualizar: ")
            valor = int(input("Escribe el nuevo precio del plato a actualizar : "))
            query = f"update menu set nombre = '{nombre}', valor = '{valor}' where idmenu = {idmenu}"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Actualizado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==4: ####### Menu - Borrar  #####
            nombre = input("Escribe el Nombre del Plato a buscar: ")
            nombre = nombre.capitalize()
            query = f"select * from menu where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idmenu = int(input("escoje el id del menu que deseas Borrar "))
            query = f"delete from menu where idmenu = {idmenu};"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Borrado Correctamente")
                lstCliente = []
                CargaInicial()

    if result == 4: ####### Menu Mesas ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMesa = utils.Menu("Mesas ", opciones)
        result = menuMesa.MostrarMenu()
        
        if result == 1: ####### Crear #####
            numero = int(input("Escribe el numero de mesa : "))
            query = f"INSERT into mesa (numero) values('{numero}');"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Creado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==2:  ####### Mesa - Buscar #####
            numero = int(input("Escribe el Numero de la mesa a buscar: "))
            query = f"select * from mesa where numero = {numero};"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}")
        
        if result == 3: ####### Mesa - Actualizar  #####
            numero = int(input("Escribe el Numero de la mesa a buscar: "))
            query = f"select * from mesa where numero = {numero};"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}")
            idmesa = int(input("escoje el id del mesa que deseas Actualizar"))
            numero = int(input("Escribe del nuevo numero de Mesa que deseas actualizar: "))
            query = f"update mesa set numero = '{numero}' where idmesa = {idmesa}"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Actualizado Correctamente")
                lstCliente = []
                CargaInicial()
        if result ==4: ####### Mesa - Borrar  #####
            numero = int(input("Escribe el Numero de la mesa a buscar: "))
            query = f"select * from mesa where numero = {numero};"
            conn  = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}")
            idmesa = int(input("escoje el id de mesa que deseas Borrar:  "))
            query = f"delete from mesa where idmesa = {idmesa};"
            conn  = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Eliminada Correctamente")
                lstCliente = []
                CargaInicial()


    