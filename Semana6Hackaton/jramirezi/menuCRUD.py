from utils import  Conexion
import utils
from modelos import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle

class EjecutarCmd:
    
    def __init__(self):
        pass
    def Insertar(self,tabla, valor):     
        
        conn  = Conexion()
        nombreCol = conn.ConsultarNombreColumnas(tabla)
        idnom = nombreCol.pop(0)  # Quitamos el Id, pues no se necesita
        campos = ""
        
        for x in nombreCol[:-1]:
            campos = campos + x + ", "           
        campos = campos +nombreCol[-1]
        query = f"INSERT INTO {tabla} ({campos}) VALUES {valor};"
        conn  = Conexion()
        if(conn.ejecutarInsert(query,tabla,idnom)):          
            print(f"se insertó : ({valor}) con {idnom} ={conn.rowId}")
            return conn.rowId
        else:
           
            return 0
class MenuList(EjecutarCmd):
    query = ""
    lstMenu = []
    def __init__(self, lstMenu=[]):
        self.lstMenu = lstMenu
        
    
    def Agregar(self, tupla):        #tupla = ('nombre', 'apellido')
        # conn = Conexion()
        # __id =conn.retornarNexttId('menu','idmenu')   #Retorna una Tupla:> (48,)
        # __id = __id[0]                                     #Extraemos la posicion [0] 
        __id = self.Insertar('menu',tupla)
        if(__id != 0):                       
            self.lstMenu.append(Cliente(__id, tupla[0], tupla[1]) )     
        else:
            print("Hubo un problema en la insersion de datos")

    def CargarData(self):
        conn = Conexion()
        self.query = 'Select * from menu'
        result = conn.consultarBDD(self.query)
        print(conn.isOkeyQuery)
        self.lstMenu = []
        if(conn.isOkeyQuery):
            for objResult in result:
                objClass = Menu(objResult[0],objResult[1], objResult[2])
                #print(objClass)
                self.lstMenu.append(objClass)
            print("Data cargada, se retorna la lista de menus ")
            return self.lstMenu
        else:
            self.lstMenu = []
            return self.lstMenu

    def Buscar(self):
        nombre = input("Escribe el Nombre del Plato a buscar: ")
        nombre = nombre.capitalize()
        query = f"select * from menu where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        if (connResult == []):
           print("No se encontraron resultados.. ")
        else:
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
        
    def Actualizar(self):
        
        idMenu = int(input("escoje el id de la Tabla menu que deseas Actualizar: "))
        nombre = input("Escribe del nuevo Nombre del Plato a actualizar: ")
        valor = int(input("Escribe el nuevo precio del plato a actualizar : "))
        query = f"update menu set nombre = '{nombre}', valor = '{valor}' where idmenu = {idMenu}"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            print("menu Actualizado Correctamente")
            for indic, itemObj in enumerate( self.lstMenu):
                
                if itemObj.idmenu == idMenu:
                    #print("Bingo :",idmenu)
                    self.lstMenu[indic] = Menu(idMenu,nombre,valor)
                    print("Lista actualizada con los valores: ",  self.lstMenu[indic])
            #lstMenu = []
            #CargaInicial()
    def Borrar(self):
        idMenu = int(input("escoje el id del Plato de la tabla menu que deseas Borrar : "))
        query = f"delete from menu where idmenu = {idMenu};"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            for indic, itemObj in enumerate( self.lstMenu):
                if itemObj.idmenu == idMenu:
                    print("Se ha Borrado Correctamente :",  self.lstMenu[indic])
                    self.lstMenu.pop(indic)
            #lstMenu = []
            #CargaInicial()
    def Get(self):
        return self.lstMenu
