from utils import  Conexion
import utils
from modelos import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle

class EjecutarCmd:
    
    def __init__(self):
        pass
    def Insertar(self,tabla, valor):     
        
        conn  = Conexion()
        nombreCol = conn.ConsultarNombreColumnas(tabla)
        idnom = nombreCol.pop(0)  # Quitamos el Id, pues no se necesita
        campos = ""
        
        for x in nombreCol[:-1]:
            campos = campos + x + ", "           
        campos = campos +nombreCol[-1]
        query = f"INSERT INTO {tabla} ({campos}) VALUES {valor};"
        conn  = Conexion()
        if(conn.ejecutarInsert(query,tabla,idnom)):          
            print(f"se insertó : ({valor}) con {idnom} ={conn.rowId}")
            return conn.rowId
        else:
           
            return 0
class MeseroList(EjecutarCmd):
    query = ""
    lstMesero = []
    def __init__(self, lstMesero=[]):
        self.lstMesero = lstMesero
        
    
    def Agregar(self, tupla):        #tupla = ('nombre', 'apellido')
        # conn = Conexion()
        # __id =conn.retornarNexttId('mesero','idmesero')   #Retorna una Tupla:> (48,)
        # __id = __id[0]                                     #Extraemos la posicion [0] 
        __id = self.Insertar('mesero',tupla)
        if(__id != 0):                       
            self.lstMesero.append(Mesero(__id, tupla[0], tupla[1]) )            
        else:
            print("Hubo un problema en la insersion de datos")

    def CargarData(self):
        conn = Conexion()
        self.query = 'Select * from mesero'
        result = conn.consultarBDD(self.query)
        print(conn.isOkeyQuery)
        self.lstMesero = []
        if(conn.isOkeyQuery):
            for objResult in result:
                objClass = Mesero(objResult[0],objResult[1],objResult[2] )
                #print(objClass)
                self.lstMesero.append(objClass)
            print("Data cargada, se retorna la lista de meseros ")
            return self.lstMesero
        else:
            self.lstMesero = []
            return self.lstMesero

    def Buscar(self):
        nombre = input("Escribe el Nombre del mesero a buscar: ")
        nombre = nombre.capitalize()
        query = f"select * from mesero where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        if (connResult == []):
           print("No se encontraron resultados.. ")
        else:
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
    
    def BuscarPorID(self, ID):
        
        query = f"select * from mesero where idmesero = {ID};"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        if (connResult == []):
           print("No se encontraron resultados.. ")
           return 0
        else:
            for objResult in connResult:
                cl = Mesero(objResult[0], objResult[1], objResult[2])
                return cl

    def Actualizar(self):
        
        idMesero = int(input("escoje el id del Mesero que deseas Actualizar: "))
        nombre = input("Escribe del nuevo Nombre del Mesero a actualizar: ")
        apellido = input("Escribe del nuevo  Apellido del mesero a actualizar : ")
        query = f"update Mesero set nombre = '{nombre}', apellido = '{apellido}' where idmesero = {idMesero}"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            print("Mesero Actualizado Correctamente")
            for indic, itemObj in enumerate( self.lstMesero):
                
                if itemObj.idmesero == idMesero:
                    #print("Bingo :",idmesero)
                    self.lstMesero[indic] = Mesero(idMesero,nombre,apellido)
                    print("Lista actualizada con los valores: ",  self.lstMesero[indic])
            #lstMesero = []
            #CargaInicial()
    def Borrar(self):
        idMesero = int(input("escoje el id del mesero que deseas Borrar : "))
        query = f"delete from mesero where idmesero = {idMesero};"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            for indic, itemObj in enumerate( self.lstMesero):
                if itemObj.idmesero == idMesero:
                    print("Se ha Borrado Correctamente :",  self.lstMesero[indic])
                    self.lstMesero.pop(indic)
            #lstMesero = []
            #CargaInicial()
    def Imprimir(self):
        for itemObj in self.lstMesero:
            print(itemObj)
        return self.lstMesero
