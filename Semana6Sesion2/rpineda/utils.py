import psycopg2
from psycopg2 import Error as pgError

import os
from time import sleep

class Conexion:
    def __init__(self, server='127.0.0.1', usr='postgres', psw='pachaqtec', bd='pchqtc10'):
        self.db = psycopg2.connect(
            host=server, user=usr, password=psw, database=bd)
        self.cursor = self.db.cursor()
        print("Se ha conectado correctamente a la base de datos")

    def consultarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            records = cur.fetchall()
            return records
        except Exception as error:
            print(error)
            return False

    def ejecutarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            self.db.commit()
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False
class Menu:
    def __init__(self, NombreMenu, ListaOpciones):
        self.NombreMenu = NombreMenu
        self.ListaOpciones = ListaOpciones

    def MostrarMenu(self):
        self.LimpiarPantalla()
        opSalir = True
        while(opSalir):
            self.LimpiarPantalla()
            print("\033[1;34m" +
                  ":::::::::::::EMPRESA PACHAQTEC::::::::::::::"+'\033[0;m')
            print("\033[1;34m"+":::::::::::::" +
                  self.NombreMenu + "::::::::::::::"+'\033[0;m')
            for (key, value) in self.ListaOpciones.items():
                print(key, " :: ", value)
            print("Salir :: 9")
            opcion = 100
            try:
                print("Escoge tu opcion")
                opcion = int(input())
            except ValueError as error:
                print("Opcion invalida deben ser numeros de 0 - 9")
            contOpciones = 0
            for (key, value) in self.ListaOpciones.items():
                if(opcion == int(value)):
                    contOpciones += 1
            if(contOpciones == 0):
                print("Escoge una opcion valida")
                sleep(5)
            else:
                opSalir = False

        return opcion

    def LimpiarPantalla(self):
        def Clear():
            # return os.system('cls')
            return os.system('clear')
        Clear()