from pymongo import MongoClient, errors
import psycopg2

import os
from time import sleep

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    CEND = '\033[0m'


class conexionBDD: 
    db  = ''
    def __init__(self,intBDD):
        self.intBDD= intBDD
#si es 1 conectarnos a Mysql, 
#si es 2 conectarnos a postgres y 
# si 3 conectarnos mongoDB
# si es cualquier otra se conecta SQLite
    
    def conexion(self):
        if(self.intBDD == 1):
            try:
                conn = mysql.connector.connect(user='root',
                                password='pachaqtec',
                                host="localhost",
                                port="3306",
                                database="rpineda")
                return conn
            except(mysql.connector.Error, Exception) as error:
                return False
            
        elif(self.intBDD == 2):
            try:
                conn = psycopg2.connect(user='postgres',
                            password='p0stgr3s',
                            host="127.0.0.1",
                            port="15432",
                            database="pchqtc10")
                return conn
            except Exception as error:
                return False
        
        elif(self.intBDD ==3):
            
# client = pymongo.MongoClient("mongodb+srv://alumnos:<password>@cluster0.b3wca.mongodb.net/<dbname>?retryWrites=true&w=majority")
# db = client.test

            uri = 'mongodb+srv://alumnos:nn3Gf0FUza9BtZjZ@cluster0.b3wca.mongodb.net/chat?retryWrites=true&w=majority'
            database = 'chat'
            try:               
                conn = MongoClient(uri)
                return conn
            except Exception as error:
                print(error)
                return False
        else:
            try:
                conn = sqlite3.connect('rpineda.db')
                return conn
            except Exception as error:
                return False
    
    def consultarBDD(self, query): 
        try:
            conexion = self.conexion()
            cur = conexion.cursor()
            cur.execute(query)
            records = cur.fetchall()
            return records
        except Exception as error:
            print(error)
            return False
        
    
    def ejecutarBDD(self, query):
        try:
            conexion = self.conexion()
            cur = conexion.cursor()
            cur.execute(query)
            conexion.commit()
            exito = True
            return exito
        except Exception as identifier:
            return False
        finally:
            conexion.close()

    def insertar_registro(self, collection, data):
        try:
            collection = self.db[collection]
            result = collection.insert_one(data)
            print(f'Inserted row: {result.inserted_id}')
            return True
        except Exception as error:
            print(error)
            return False
    
    def insertar_registros(self,collection, data):
        collection = self.db[collection]
        result = collection.insert_many(data)
        print(f'Inserted rows: {result.inserted_ids}')        

    def obtener_registros(self, collection, condition={}):
        try:
            conexion = self.conexion()
            collection = conexion.db.mensajes
            data = collection.find(condition)
            return list(data)
        except Exception as error:
            print(error)
            sleep(5)
            return False
        
    def obtener_registro(self, collection, condition={}):
        conexion = self.conexion()
        collection = conexion.db[collection]
        data = collection.find_one(condition)
        return list(data)

    def actualizar_registro(self, collection, condition, change):
        collection = self.db[collection]
        collection.update_one(condition, {
            '$set': change
        })
        print(f'Se actualizo un registro')

    def eliminar_registro(self, collection, condition):
        collection = self.db[collection]
        collection.delete_one(condition)
        print(f'Se elimino un registro')
        
    def cerrar_conexion(self):
        self.db.close()
        print('Se cerro la conexión con exito !')
        return True

class Menu:
    def __init__(self, nombreMenu, listaOpciones):
        self.nombreMenu = nombreMenu
        self.listaOpciones = listaOpciones

    def mostrarMenu(self):
        self.limpiarPantalla()
        opSalir = True
        while(opSalir):
            self.limpiarPantalla()
            print(f"""{color.BLUE}
::::::::::::: EMPRESA MONTENEGRO ::::::::::::::
::::::::::::: {self.nombreMenu} ::::::::::::::
                  {color.CEND}""")
            
            for (key, value) in self.listaOpciones.items():
                print(value, "->", key)
            opcion = 100
            print("9 -> Salir")
            try:
                opcion = int(input(color.CYAN+"Escoge tu opcion: "+color.CEND))
            except ValueError as error:
               # self.__log.error(error)
                print(color.RED+"Opcion invalida deben ser numeros del 0 al 2"+color.CEND)
            contOpciones = 0
            for (key, value) in self.listaOpciones.items():
                if(opcion == int(value) or opcion == 9):
                   contOpciones += 1
            if(contOpciones == 0):
                print(color.RED+"Escoge una opcion valida"+color.CEND)
               # self.__log.debug("No escoje opion")
                sleep(3)
            else:
                opSalir = False

        return opcion

    def limpiarPantalla(self):
        def clear():
            #return os.system('cls')
            return os.system('clear')
        clear()