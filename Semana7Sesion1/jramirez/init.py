from utils import Conexion, Menu
from time import sleep

conn = Conexion("mongodb://localhost:27017","Pinturas")


opMenuPrincipal = {"\t-Agregar":1,"\t-Buscar":2, "\t-Actualizar":3, "\t-Eliminar":4}

menuPrincipal = Menu("Menu Principal", opMenuPrincipal)
salirMenu = True
conn = Conexion("mongodb://localhost:27017","Pinturas")

while salirMenu:
    result = menuPrincipal.mostrarMenu()
    if(result == 1):
        color = input("Ingrese el color :")
        codigo = input("Ingrese el codigo : ")
        marca = input("Ingrese la marca: ")
        referencia = input("Ingrese la referencia: ")
        tipo = input("Ingrese el tipo de pintura : ")
        dicPintura = {
            "color": color,
            "codigo": codigo,
            "marca": marca,
            "referencia": referencia,
            "tipo": tipo   
        }
        #conn = Conexion("mongodb://localhost:27017","Pinturas")
        if(conn.insertar_registro("pinturas",dicPintura)):
            print("Se ha insertado correctamente: ")
            sleep(3)
        else:
            print("hubo un error")
        
    if(result == 2):
        color = input("Ingrese el color a buscar: ")
        query = { "color": { "$regex": f'{color}', "$options" :'i' } }
        print(query)
        dicCondicion = { "color": f'{color}' }
        result = conn.obtener_registros("pinturas",query)
        # if not result:
        #     print("hubo un error")
        #     sleep(3)
        # else:
        #print(result)
        for obj in result:
            print(f"Color: {obj['color']}, Codigo: {obj['codigo']}, Referencia: {obj['referencia']}")
        sleep(5)

    if(result == 3):
        color = input("Ingrese el color a actualizar: ")   
        newColor = input("Ingrese el nuevo color: ") 
        conn.actualizar_registro("pinturas",{"color": color}, {"color":newColor})

    if(result == 4):
        color = input("Ingrese el color a borrar: ") 
        conn.eliminar_registro("pinturas", {"color": color})
    
    if(result == 9):
        print("Salir") 
        salirMenu = False
