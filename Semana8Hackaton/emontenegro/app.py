import os
from flask import Flask, request
from flask_orator import Orator, jsonify
from orator.orm import belongs_to, has_many, belongs_to_many



# Configuration
DEBUG = True
# ORATOR_DATABASES = {
#     'default': 'postgres',
#     'postgres': {
#         'driver': 'postgres',
#         'host': 'localhost',
#         'database': 'chat',
#         'user': 'postgres',
#         'password': 'pachaqtec',
#         'prefix': ''
#     }
# }
ORATOR_DATABASES = {
    'default': 'mysql',
    'mysql': {
        'driver': 'mysql',
        'host': 'localhost',
        'port': 23306,
        'database': 'chat',
        'user': 'root',
        'password': 'mysql',
        'prefix': ''
    }
}

# Creating Flask application
app = Flask(__name__)
app.config.from_object(__name__)

# Initializing Orator
db = Orator(app)

class User(db.Model):
    __fillable__ = ['username', 'email', 'password']
    __hidden__ = ['pivot']
    
    @belongs_to_many(
        'meets',
        'user_id', 'sala_id',
        with_timestamps=True
    )

    def usuarioSalas(self):
        return Sala

    def is_usuarioSala(self, sala):
        return self.usuarioSalas().where('sala_id', sala.id).exists()
    
    def entrarSala(self, sala):
        if not self.is_usuarioSala(sala):
            self.usuarioSalas().attach(sala)

    def salirSala(self, sala):
        if self.is_usuarioSala(sala):
            self.usuarioSalas().detach(sala)



class Sala(db.Model):
    __fillable__ = ['nombre']
    

    def is_salaUsuario(self, sala):
        return self.salasUsuario().where('sala_id', sala.id).exists()



class Message(db.Model):
    __fillable__ = ['content']

    @belongs_to
    def user(self):
        return User





"""
@has_many
    def messages(self):
        return Message
"""


"""
======== GENERAL - CONTROLLER ==========
"""
@app.route('/', methods=['GET'])
def home():
    return "Hola desde Flask ORATOR"


"""
======== USER - CONTROLLER ==========
"""
@app.route('/users', methods=['POST'])
def create_user():
    user = User.create(**request.get_json())
    return jsonify(user)

@app.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.find_or_fail(user_id)
    return jsonify(user)

@app.route('/users', methods=['GET'])
def get_all_users():
    users = User.all()
    return jsonify(users)

@app.route('/users/<int:user_id>', methods=['PATCH'])
def update_user(user_id):
    user = User.find_or_fail(user_id)
    user.update(**request.get_json())
    return jsonify(user)
"""
@app.route('/users/<int:user_id>/messages', methods=['GET'])
def get_user_messages(user_id):
    user = User.find_or_fail(user_id)
    return jsonify(user.messages)

@app.route('/users/<int:user_id>/messages', methods=['POST'])
def create_message(user_id):
    user = User.find_or_fail(user_id)
    message = user.messages().create(**request.get_json())
    return jsonify(message)
"""
@app.route('/users/<int:user_id>/salas', methods=['GET'])
def get_user_salas(user_id):
    user = User.find_or_fail(user_id)
    return jsonify(user.usuarioSalas)

@app.route('/users/<int:user_id>/ingresarsala/<int:sala_id>', methods=['PUT'])
def entrarSala(user_id, sala_id):
    user = User.find_or_fail(user_id)
    sala = Sala.find_or_fail(sala_id)
    user.entrarSala(sala)
    return app.response_class('No Content', 204)

@app.route('/users/<int:user_id>/salirsala/<int:sala_id>', methods=['PUT'])
def salirSala(user_id, sala_id):
    user = User.find_or_fail(user_id)
    sala = Sala.find_or_fail(sala_id)
    user.salirSala(sala)
    return app.response_class('No Content', 204)


"""
======== MESSAGES - CONTROLLER ==========
"""
@app.route('/messages/<int:message_id>', methods=['GET'])
def get_message(message_id):
    message = Message.find_or_fail(message_id)
    return jsonify(message)

@app.route('/messages/<int:message_id>', methods=['PATCH'])
def update_message(message_id):
    message = Message.find_or_fail(message_id)
    message.update(**request.get_json())
    return jsonify(message)

@app.route('/messages/<int:message_id>', methods=['DELETE'])
def delete_message(message_id):
    message = Message.find_or_fail(message_id)
    message.delete()
    return app.response_class('No Content', 204)


"""
======== SALAS - CONTROLLER ==========
"""
@app.route('/salas', methods=['POST'])
def create_sala():
    sala = Sala.create(**request.get_json())
    return jsonify(sala)

@app.route('/salas/<int:sala_id>/user', methods=['GET'])
def get_sala_iser(sala_id):
    sala = Sala.find_or_fail(sala_id)
    return jsonify(sala.salasUsuario)



if __name__ == '__main__':
    app.run(debug=True, port=8050)
