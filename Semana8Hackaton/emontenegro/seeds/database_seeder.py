from orator.seeds import Seeder
from .meets_table_seeder import MeetsTableSeeder
from .salas_table_seeder import SalasTableSeeder
from .users_table_seeder import UsersTableSeeder
from .messages_table_seeder import MessagesTableSeeder

class DatabaseSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.call(UsersTableSeeder)
        self.call(SalasTableSeeder)
        self.call(MeetsTableSeeder)
        self.call(MessagesTableSeeder)

