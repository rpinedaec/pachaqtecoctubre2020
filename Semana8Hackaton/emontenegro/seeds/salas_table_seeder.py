from orator.seeds import Seeder


class SalasTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('salas').insert({
            'nombre': 'Sala1'
        })
        self.db.table('salas').insert({
            'nombre': 'Sala2'
        })
        self.db.table('salas').insert({
            'nombre': 'Sala3'
        })

