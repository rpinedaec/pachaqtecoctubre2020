from orator.seeds import Seeder


class MeetsTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        with self.db.transaction():
            karen = self.db.table('users').where('username', 'karen').first()
            roberto = self.db.table('users').where('username', 'roberto').first()
            david = self.db.table('users').where('username', 'david').first()

            self.db.table('meets').insert([
                {'sala_id': 1, 'user_id': roberto.id},
                {'sala_id': 2, 'user_id': david.id},
                {'sala_id': 3, 'user_id': karen.id},
            ])

