from orator.seeds import Seeder


class MessagesTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        karen = self.db.table('users').where('username', 'karen').first()
        roberto = self.db.table('users').where('username', 'roberto').first()
        david = self.db.table('users').where('username', 'david').first()

        self.db.table('messages').insert({
            'content': 'Karen\'s message.',
            'meet_id': karen['id']
        })

        self.db.table('messages').insert({
            'content': 'Roberto\'s message.',
            'meet_id': roberto['id']
        })

        self.db.table('messages').insert({
            'content': 'David\'s message.',
            'meet_id': david['id']
        })

