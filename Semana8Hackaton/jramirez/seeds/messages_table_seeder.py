from orator.seeds import Seeder


class MessagesTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        

        self.db.table('messages').insert({
            'meets_id': 1,
            'content': 'Roberto\'s message. Sala1'
        })
        self.db.table('messages').insert({
            'meets_id': 2,
            'content': 'Latina\'s message. Sala1'
        })
        self.db.table('messages').insert({
            'meets_id': 3,
            'content': 'Julio\'s message.Sala1'
        })
        self.db.table('messages').insert({
            'meets_id': 4,
            'content': 'Julio\'s message. Sala2'
        })
        self.db.table('messages').insert({
            'meets_id': 5,
            'content': 'Jorge\'s message. Sala2'
        })
        self.db.table('messages').insert({
            'meets_id': 6,
            'content': 'Gringo\'s message. Sala2'
        })