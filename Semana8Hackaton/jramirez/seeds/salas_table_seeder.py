from orator.seeds import Seeder


class SalasTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('salas').insert({
            'nombre': 'Solo coders'
        })
        self.db.table('salas').insert({
            'nombre': 'FullVaso'
        })
        self.db.table('salas').insert({
            'nombre': 'Las locas de la cuadra'
        })
        self.db.table('salas').insert({
            'nombre': 'Amor del Bueno'
        })
        self.db.table('salas').insert({
            'nombre': 'No se Aceptan Hombres'
        })

