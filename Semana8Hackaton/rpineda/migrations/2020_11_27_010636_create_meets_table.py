from orator.migrations import Migration


class CreateMeetsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('meets') as table:
            table.increments('id')
            table.integer('user_id').unsigned()
            table.integer('sala_id').unsigned()
            table.timestamps()

            table.foreign('user_id').references('id').on('users').on_delete('cascade')
            table.foreign('sala_id').references('id').on('salas').on_delete('cascade')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('meets')
