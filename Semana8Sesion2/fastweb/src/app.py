  
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:123456@localhost:3306/flaskmysql'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(70), unique=True)
    description = db.Column(db.String(100))

    def __init__(self, title, description):
        self.title = title
        self.description = description

db.create_all()

class TaskSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'description')


task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)

@app.route('/tasks', methods=['Post'])
def create_task():
   #print(request.json)
   #return 'recibido'
    title = request.json['title']
    description = request.json['description']
    new_task = Task(title, description)

    db.session.add(new_task)
    db.session.commit()

    return task_schema.jsonify(new_task) # retorna el objeto en formato json

#Query con todos los elementos de la tabla, usa método GET para realizar la consulta
@app.route('/tasks', methods = ['GET'])
def get_tasks():
    all_tasks = Task.query.all()
    result = tasks_schema.dump(all_tasks)
    return jsonify(result)

#Query: "Selec * from task where id = <id> ", hace un select con el id que viene de la URL,
#usa el método GET para consultar.
@app.route('/tasks/<id>', methods=['GET'])
def get_task(id):
   task = Task.query.get(id)            #retorna un objeto del tipo task
   print("estoy imprimendo :", task.title)      #impimimos la propidad title
   return task_schema.jsonify(task)     #Convertimos el objeto en json 

#Vamos Actualizar por id los campos de la tabla&objeto task
#Se usa el método PUT para actualizar todos los campos.
@app.route('/tasks/<id>', methods =['PUT'])
def update_task(id):
   task = Task.query.get(id)        #con esto me traigo el objeto
   # junto con la URL debe venir un Json con los nuevos valores, 
   #Recuperamos el json que nos envían: c
   titulo = request.json['title']              #Recuperamos el campo 'titulos del json y lo asignamos a la variable titulo
   descripcion = request.json['description']    #Recuperamos el campo 'description del json y los asignamos a la variable descripción
   
   #Vamos a actualizar los campos del objeto con los valores recuperados del json:
   task.title = titulo
   task.  = descripcion

   #Hacemos un commit, para que actualice la base de datos:
   db.session.commit()

   return task_schema.json(task)

@app.route('/tasks/<id>', methods =['DELETE'])
def method_name():
    objTask = Task.query.get(id)
    db.session.delete(objTask)
    db.session.commit()
    return task_schema.jsonify(objTask)

if __name__ == "__main__":
    app.run(debug=True)