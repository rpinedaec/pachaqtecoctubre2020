import os
from flask import Flask, request
from flask_orator import Orator, jsonify

# Configuration
DEBUG = True
ORATOR_DATABASES = {
    'default': 'mysql',
    'mysql': {
        'driver': 'mysql',
        'host': 'localhost',
        'database': 'orator',
        'user': 'root',
        'password': 'pachaqtec',
        'prefix': ''
    }
}

# Creating Flask application
app = Flask(__name__)
app.config.from_object(__name__)

# Initializing Orator
db = Orator(app)



class User(db.Model):
    __fillable__ = ['name', 'email']

@app.route('/', methods=['GET'])
def home():
    return "Hola desde Flask ORATOR"


@app.route('/users', methods=['POST'])
def create_user():
    user = User.create(**request.get_json())
    return jsonify(user)

@app.route('/users/<int:user_id>/messages', methods=['GET'])
def get_user_messages(user_id):
    user = User.find_or_fail(user_id)
    return jsonify(user.messages)

@app.route('/users', methods=['GET'])
def get_all_users():
    users = User.all()

    return jsonify(users)

if __name__ == '__main__':
    app.run()