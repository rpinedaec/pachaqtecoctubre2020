import os
from flask import Flask, request
from flask_orator import Orator, jsonify
from orator.orm import belongs_to, has_many, belongs_to_many

# Configuration
DEBUG = True
ORATOR_DATABASES = {
    'default': 'postgres',
    'postgres': {
        'driver': 'postgres',
        'host': 'localhost',
        'database': 'chat',
        'user': 'postgres',
        'password': '123456',
        'prefix': ''
    }
}

# Creating Flask application
app = Flask(__name__)
app.config.from_object(__name__)

# Initializing Orator
db = Orator(app)


class User(db.Model):
    __fillable__ = ['name', 'email']
    __hidden__ = ['pivot']

    


if __name__ == '__main__':
    app.run(debug=True, port=8050)
