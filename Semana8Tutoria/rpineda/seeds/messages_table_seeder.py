from orator.seeds import Seeder


class MessagesTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        karen = self.db.table('users').where('username', 'karen').first()
        roberto = self.db.table('users').where('username', 'roberto').first()
        david = self.db.table('users').where('username', 'david').first()

        self.db.table('messages').insert({
            'content': 'Karen\'s message.',
            'user_id': karen['id']
        })

        self.db.table('messages').insert({
            'content': 'Roberto\'s message.',
            'user_id': roberto['id']
        })

        self.db.table('messages').insert({
            'content': 'David\'s message.',
            'user_id': david['id']
        })

